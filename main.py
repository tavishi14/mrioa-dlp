# With Exceptional Handling
import os
import uuid
from google.cloud import storage
import yaml
import logging
import vertexai
import fitz
from PIL import Image
from PyPDF2 import PdfMerger
import io
import mimetypes
import google.cloud.dlp
from flask import Flask, request, jsonify
import shutil

# Setup logging
log_dir = 'logs'
os.makedirs(log_dir, exist_ok=True)
log_file_path = os.path.join(log_dir, 'redaction.log')
logging.basicConfig(
    filename=log_file_path,
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
# Load configuration
try:
    with open("config/config.yml", "r") as ymlfile:
        cfg = yaml.load(ymlfile, Loader=yaml.Loader)
    logging.info("Config loaded successfully")
except Exception as e:
    logging.error(f"Error loading config file: {e}")
    raise e

# Initialize Google Cloud Storage client
try:
    storage_client = storage.Client()
    dlp_client = google.cloud.dlp_v2.DlpServiceClient()
    PROJECT_ID = cfg['GCP']['PROJECT_ID']
    LOCATION = cfg['GCP']['LOCATION']
    vertexai.init(project=PROJECT_ID, location="us-central1")
    input_bucket = cfg['GCP']['INPUT_BUCKET']
    output_bucket = cfg['GCP']['OUTPUT_BUCKET']
    
except Exception as e:
    logging.error(f"Error initializing GCP clients or loading config values: {e}")
    raise e

# we will get this file from the cloud function trigger


# Define the function to split PDF and save images locally
def split_pdf(input_bucket, input_file, dpi=300):
    try:
        print(f"Downloading file: gs://{input_bucket}/{input_file}")
        bucket = storage_client.get_bucket(input_bucket)
        blob = bucket.get_blob(input_file)
        if not blob:
            raise FileNotFoundError("File not found in the specified bucket.")
        
        downloaded_filename = f"/temp/{str(uuid.uuid4())}.pdf"
        blob.download_to_filename(downloaded_filename)
        print(f"Input file downloaded from GCS to {downloaded_filename}")
        
        images = fitz.open(downloaded_filename)
        uploaded_images = []
        # Ensure the output directory exists
        
        output_directory="splitted_images"
        os.makedirs(output_directory, exist_ok=True)

        for i in range(len(images)):
            page = images.load_page(i)
            pix = page.get_pixmap(dpi=dpi)
            file_name = os.path.basename(input_file)
            output_folder = os.path.splitext(file_name)[0]
            output_filename = f"{output_directory}/{output_folder}_page-{str(i).zfill(4)}.jpg"
            pix.save(output_filename)
            
            
            uploaded_images.append(output_filename)
            print(f"Image saved locally: {output_filename}")
        os.remove(downloaded_filename)
        logging.info(f"Successfully split PDF {input_file} and saved locally")
        return uploaded_images, output_folder
    except Exception as e:
        logging.error(f"Error processing PDF {input_file}: {e}")
        return []


def redact_image(project, input_filename, output_filename, include_quotes=True, mime_type=None):
    try:
        if mime_type is None:
            mime_guess = mimetypes.MimeTypes().guess_type(input_filename)
            mime_type = mime_guess[0] or "application/octet-stream"
        supported_content_types = {
            None: 0, "image/jpeg": 1, "image/bmp": 2, "image/png": 3, "image/svg": 4, "text/plain": 5,
        }
        content_type_index = supported_content_types.get(mime_type, 0)
        with open(input_filename, mode="rb") as f:
            byte_item = {"type_": content_type_index, "data": f.read()}
        parent = f"projects/{project}"
        info_types = [{"name": "PERSON_NAME"}, {"name": "LAST_NAME"}, {"name": "PHONE_NUMBER"}, {"name": "DATE_OF_BIRTH"},
                      {"name": "MEDICAL_RECORD_NUMBER"}, {"name": "LOCATION"}]
        min_likelihood = google.cloud.dlp_v2.Likelihood.LIKELIHOOD_UNSPECIFIED
        max_findings = 0
        include_quote = True
        inspect_config = {
            "info_types": info_types,
            "min_likelihood": min_likelihood,
            "include_quote": include_quote,
            "limits": {"max_findings_per_request": max_findings},
        }
        response = dlp_client.redact_image(
            request={
                "parent": parent,
                "inspect_config": inspect_config,
                "byte_item": byte_item,
                "include_findings": True
            })
        with open(output_filename, mode="wb") as f:
            f.write(response.redacted_image)
        logging.info(f"Successfully redacted image {input_filename}")
        return response.inspect_result
    except Exception as e:
        logging.error(f"Error redacting image {input_filename}: {e}")
        return None
    
def images_redaction_process(directory):
    """
    Process each image in a directory for redaction.
    """
    for filename in os.listdir(directory):
        if filename.endswith(".jpg"):
            input_path = os.path.join(directory, filename)
            output_directory="redacted_images"
            os.makedirs(output_directory, exist_ok=True)
            output_path = os.path.join(output_directory, "redacted_" + filename)
            findings = redact_image(cfg['GCP']['PROJECT_ID'], input_path, output_path)
            logging.info("Processed file: {} with findings: {}".format(output_path, findings))

def concatenate_images_into_pdf(local_folder, original_input_filename, output_bucket):

    print(f"Concatenating images from local folder: {local_folder}")

    # Form the output file name based on the original input file name
    output_file = os.path.splitext(original_input_filename)[0] + "_redacted.pdf"

    # Initialize PDF merger
    pdf_merger = PdfMerger()

    # Collect all image files from the specified local directory
    image_files = [f for f in os.listdir(local_folder) if f.endswith('.jpg')]
    image_files.sort()  # Sort files by name to maintain order
    print("Sorting and processing files")

    # Convert each image to a PDF page and add it to the merger
    for image_filename in image_files:
        local_image_path = os.path.join(local_folder, image_filename)
        print(f"Processing file {local_image_path}")

        # Convert image to PDF
        doc = fitz.open(local_image_path)  # Open image
        pdf_bytes = doc.convert_to_pdf()  # Convert image to PDF
        pdf_stream = io.BytesIO(pdf_bytes)  # Convert bytes to a file stream

        # Add the current image's PDF stream to the merger
        pdf_merger.append(pdf_stream)

    # Save the final concatenated PDF to a temporary file
    final_pdf_path = f"{str(uuid.uuid4())}.pdf"
    pdf_merger.write(final_pdf_path)
    pdf_merger.close()
    print(f"Final PDF created at {final_pdf_path}, uploading to GCS")

    # Upload final concatenated PDF to GCS bucket
    output_bucket = storage_client.get_bucket(output_bucket)
    blob = output_bucket.blob(output_file)
    blob.upload_from_filename(final_pdf_path)
    print(f"Concatenated PDF uploaded to: gs://{output_bucket.name}/{output_file}")

    # Cleanup local PDF file
    os.remove(final_pdf_path)

    return {
        "full_file": f"gs://{output_bucket.name}/{output_file}",
        "file": output_file,
        "bucket": output_bucket.name
    }

def remove_directory(path):
    """Remove the directory specified by 'path' and all its contents."""
    try:
        shutil.rmtree(path)
        print(f"Directory '{path}' has been removed successfully.")
    except Exception as e:
        print(f"Failed to remove directory '{path}'. Error: {e}")


def main(input_file):
    
    try:
        uploaded_images = split_pdf(input_bucket, input_file)
        logging.info("pdf splitted sucessfully into images and stored into local folder")
        
    except Exception as e:
        logging.error(f"Error getting in converting PDF to file: {e}")
    try:
        images_redaction_process('splitted_images')
        logging.info("Redaced images ")
        
    except Exception as e:
        logging.error(f"Error in Redacting Images process: {e}")

    try:
        local_folder = "redacted_images"
        result = concatenate_images_into_pdf(local_folder, input_file, output_bucket)
        logging.info("sucessfully concatenated Images into pdf")

    except Exception as e:
        logging.error(f"Error in concatenating Images into pdf: {e}")
    
    try:
        remove_directory("splitted_images")
    
        logging.info("sucesslly removed local directory splitted_images ")
    except Exception as e:
        logging.error(f"Error in removing local directory splitted_images: {e}")
    
    try:
        remove_directory("redacted_images")
        logging.info("sucesslly removed local directory redacted_images ")
    except Exception as e:
        logging.error(f"Error in removing local directory redacted_images: {e}")


if __name__ == "__main__":
    main()
    
