import os
import yaml
import vertexai
import logging
from google.cloud import storage
import google.cloud.dlp
from flask import Flask, request, jsonify
from flask_cors import CORS
from main import main

# Set up logging
log_dir = 'logs'
os.makedirs(log_dir, exist_ok=True)
log_file_path = os.path.join(log_dir, 'DLP.log')
logging.basicConfig(
    filename=log_file_path,
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
try:
    with open("config/config.yml", "r") as ymlfile:
        cfg = yaml.load(ymlfile, Loader=yaml.Loader)
    logging.info("Config loaded successfully")
except Exception as e:
    logging.error(f"Error loading config file: {e}")
    raise e

app = Flask(__name__)
CORS(app)


@app.route('/process', methods=['GET'])
def process_files():
    """ Endpoint to trigger processing based on the provided PDF file paths. """
    try:
        
        input_file = request.args.get('fileName')
        main(input_file) 
        return jsonify({'message': 'Processing initiated successfully1'}), 200
    except Exception as e:
        logging.error(f"Error during processing: {e}")
        return jsonify({'error': str(e)}), 500
    
if __name__ == '__main__':
    app.run(debug=True,host= '0.0.0.0', port=5001)
