# Use the official Python image
FROM python:3.10-slim
 
# Set the working directory to /app
RUN mkdir app
WORKDIR /app

COPY requirements.txt ./

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Copy the current directory contents into the container at /app
COPY . .

# ENV ANONYMIZED_TELEMETRY=False

EXPOSE 8080
# Run main.py when the container launches
CMD echo "" &&  exec gunicorn --bind :8080 --workers 1 --timeout 0 app:app
